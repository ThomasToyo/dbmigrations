FROM python:3.11-slim-buster
ENV PYTHONUNBUFFERED=1	
RUN apt-get update 
RUN apt-get install -y gcc 
RUN apt-get install -y default-libmysqlclient-dev
ENV UNBUFFERED=1
WORKDIR /app
COPY . /app
RUN pip install -r requirements.txt
COPY . .
CMD ["./entrypoint.sh"]
